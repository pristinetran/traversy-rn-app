/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
// import Routes from "./src/app/Routes";
import { Provider } from 'react-redux'
// import HomeScreen from "./src/examples/stack-navigation/HomeScreen/index.js";
import {View} from 'react-native';
import {Header} from './src/LibraryComp/components/common';
import LibraryList from './src/LibraryComp/components/LibraryList';
import store from './src/store'

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <Header headerText="Tech Stack" />
          <LibraryList />
        </View>
      </Provider>
    )
  }
}
