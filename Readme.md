## Instructions:
- Clone/Download the app at `master` branch or latest `release` branch
- Ensure `react-native-cli` is installed, do a `npm i -g react-native-cli` if not. This proj was scaffolded 09/11/2018 with Facebook official react-native-cli through `react-native init myRNapp`
- To start the app, Run `npm i`, wait until completion.
- Then, run `npm start`
- Open a new terminal tab at the project directory, run `react-native run-ios` for IOS or `react-native run-android` for Android development.

You can specify the device the simulator should run with the --simulator flag, followed by the device name as a string. The default is "iPhone 6". If you wish to run your app on an iPhone 4s, just run react-native run-ios --simulator="iPhone 4s".

The device names correspond to the list of devices available in Xcode. You can check your available devices by running xcrun simctl list devices from the console. - Android, should be the same.

*** Always remember that,
For mobile dev, run npm start to initiate the packager (Metro Bundler first) then run react-native run-ios to kickstart the Xcode launchpad of IPhone6 emulator.

Questions, email to hivetechlogic@gmail.com


Ref:
https://snack.expo.io/@react-navigation/going-back-v2
