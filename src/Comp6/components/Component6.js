import React, {Component} from 'react';
import {Button, StyleSheet} from 'react-native';
import {Container, Text, View} from 'native-base';
import HeaderComp from '../../app/header/Header';

export default class Component6 extends Component {
  render() {
    const { navigation } = this.props;
    const user = navigation.getParam('user', {});
    console.log('user', user);

    return (
      <Container>
        <HeaderComp onPressHandler={this.props.navigation.openDrawer} title={'Comp6'} />
        <View>
          <Text>User Details</Text>
          {Object.keys(user).length > 0 &&
          <View>
            <Text>{user.name}</Text>
            <Text style={styles.rowItalicText}>{'    ' + user.email}</Text>
          </View>
          }
          <Button
            title="Go back"
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  rowItalicText: {
    fontStyle: 'italic',
    textAlign: 'right'
  }
})

Component6.defaultProps = {}

Component6.propTypes = {};
