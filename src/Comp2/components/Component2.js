import React, {Component} from 'react';
import {StyleSheet, TouchableHighlight, TouchableOpacity} from 'react-native';
import {
  View,
  Text,
  Container
} from "native-base";
import HeaderComp from '../../app/header/Header';

export default class Component2 extends Component {

  onPress() {
    console.log('Area Pressed');
  }

  onPress2() {
    console.log('Area Pressed');
  }

  render() {
    return (
      <Container>
        <HeaderComp onPressHandler={this.props.navigation.openDrawer} title={'Comp2'} />
        <View style={styles.myView}>
          <Text style={styles.myText}>This is Comp2</Text>
          <View style={styles.container}>
            <TouchableHighlight
              onPress={this.onPress}
              style={styles.v1}
              underlayColor={'lightblue'}
            >
              <View>
                <Text>View 1</Text>
              </View>
            </TouchableHighlight>
            <TouchableOpacity
              onPress={this.onPress2}
              style={styles.v2}
            >
              <View style={styles.v2}>
                <Text>View 2</Text>
              </View>
            </TouchableOpacity>
            <View style={styles.v3}>
              <Text style={styles.v3text}>View 3</Text>
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  myView: { backgroundColor: 'lightblue' },
  myText: { color: 'green' },
  container: {
    flexDirection: 'row',
    height: 100
  },
  v1: {
    flex: 1,
    backgroundColor: 'lightgreen',
    padding: 10
  },
  v2: {
    flex: 1,
    backgroundColor: 'lightgrey',
    padding: 10
  },
  v3: {
    flex: 1,
    backgroundColor: 'lightpink',
    padding: 10
  },
  v3text: {
    color: 'yellow'
  }
})


Component2.defaultProps = {};

Component2.propTypes = {};
