import React, {Component} from 'react';
import {
  Text,
  Container,
} from "native-base";
import HeaderComp from "../../app/header/Header";

export default class Component1 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      name: 'John Doe',
      message: this.props.message
    }
  }

  render() {
    return (
      <Container>
        <HeaderComp onPressHandler={this.props.navigation.openDrawer} title={'Comp1'} />

        <Text>Hello {this.state.message} {this.props.age}</Text>
        <Text>{this.state.name}</Text>
      </Container>
    );
  }
}

Component1.defaultProps = {
  age: 36
}

Component1.propTypes = {};
