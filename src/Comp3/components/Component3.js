import React, {Component} from 'react';
import {Switch, TextInput} from 'react-native';
import {
  View,
  Text,
  Container,
} from "native-base";
import HeaderComp from "../../app/header/Header";

export default class Component3 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      textValue: 'Lorem Isum',
      inputValue: 'vitae',
      switchValue: false
    }
    this.onChangeTextField1 = this.onChangeTextField1.bind(this);
  }

  onChangeTextField1(value) {
    this.setState({textValue: value})
  }
  onChangeTextField2 = (value) => this.setState({ inputValue: value })

  onSubmit = () => console.log('Input submitted ...');
  onSwitchChange = (value) => this.setState({ switchValue: value }, () => console.log('switchValue', this.state.switchValue))

  render() {
    return (
      <Container>
        <HeaderComp onPressHandler={this.props.navigation.openDrawer} title={'Comp3'} />

        <Text> Demo: {"\n"} This is a Comp 3 {"\n"}</Text>
        <View>
          <Text style={{ color: 'grey' }}>With state value when changing text only (WITHOUT submit editing) {"\n"}</Text>
          <TextInput
            placeholder={'Enter Text 1'}
            value={this.state.textValue}
            onChangeText={this.onChangeTextField1}
          />
          <Text style={{ color: 'lightblue' }}>{this.state.textValue}</Text>
        </View>
        <View>
          <Text style={{ color: 'grey' }}>{"\n"} {"\n"} With state value when changing text AND submit editing) {"\n"}</Text>
          <TextInput
            placeholder={'Enter Text 2'}
            value={this.state.inputValue}
            onChangeText={this.onChangeTextField2}
            onSubmitEditing={this.onSubmit}
          />
          <Text style={{ color: 'lightblue' }}>{this.state.inputValue}</Text>
        </View>
        <Switch
          value={this.state.switchValue}
          onValueChange={this.onSwitchChange}
        />
      </Container>
    );
  }
}

Component3.defaultProps = {}

Component3.propTypes = {};
