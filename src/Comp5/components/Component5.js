import React, {Component} from 'react';
import {ListView, StyleSheet, TouchableHighlight} from 'react-native';
import HeaderComp from '../../app/header/Header';
import {View, Text, Container} from 'native-base';

export default class Component5 extends Component {
  constructor(props, context) {
    super(props, context);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      userDataSource: ds
    }
  }

  fetchUsers = () => fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then(res => {
      this.setState({
        userDataSource: this.state.userDataSource.cloneWithRows(res)
      })
    })

  onPress = (user) => () => {
    this.props.navigation.navigate('Comp6', { user })
  }

  renderRow = (user, sectionId, rowId, highlightRow) => (
    <TouchableHighlight onPress={this.onPress(user)}>
      <View style={styles.row}>
        <Text style={styles.rowText}>{user.name}</Text>
        <Text style={styles.rowItalicText}>{user.email.toLowerCase()}</Text>
      </View>
    </TouchableHighlight>
  )

  componentDidMount() {
    this.fetchUsers();
  }

  render() {
    return (
      <Container>
        <HeaderComp onPressHandler={this.props.navigation.openDrawer} title={'Comp5'} />
        <View>
          <Text>Component 5</Text>
          <ListView
            dataSource={this.state.userDataSource}
            renderRow={this.renderRow}
          />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
    backgroundColor: '#f4f4f4',
    marginBottom: 3
  },
  rowText: {
    flex: 1,
    textAlign: 'left'
  },
  rowItalicText: {
    fontStyle: 'italic',
    textAlign: 'right'
  }
})


Component5.defaultProps = {}

Component5.propTypes = {};
