const initialState = {
  selectedId: ''
}

export default (state = initialState, action) => {
  switch (action.type) {

    case 'SELECT_LIBRARY':
      return {...state, selectedId: action.payload}

    default:
      return state;
  }
};
