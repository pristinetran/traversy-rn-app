import React, {Component} from 'react';
import {ListView, StyleSheet} from 'react-native';
import {Text, View, Container} from 'native-base';
import HeaderComp from '../../app/header/Header';

const users = [
  { name: 'John Doe' },
  { name: 'Brad Traversy' },
  { name: 'Jane Doe' },
  { name: 'Jeanny Doe' }
]

export default class Component4 extends Component {
  constructor(props, context) {
    super(props, context);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      userDataSource: ds.cloneWithRows(users)
    }

    // this.renderRow = this.renderRow.bind(this)
  }

  renderRow = (user, sectionId, rowId, highlightRow) => (
    <View style={styles.row}>
      <Text style={styles.rowText}>{user.name}</Text>
    </View>
  )

  render() {
    return (
      <Container>
        <HeaderComp onPressHandler={this.props.navigation.openDrawer} title={'Comp4'} />
        <ListView
          dataSource={this.state.userDataSource}
          renderRow={this.renderRow}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
    backgroundColor: '#f4f4f4',
    marginBottom: 3
  },
  rowText: {
    flex: 1
  }
})

Component4.defaultProps = {}

Component4.propTypes = {};
