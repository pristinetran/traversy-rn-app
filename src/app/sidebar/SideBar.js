import React from "react";
import { Image } from "react-native";
import {
  Text,
  Container,
  List,
  ListItem,
  Content
} from "native-base";
import {routes} from "../Routes";

export default class SideBar extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <Image
            source={{
              uri:
                "https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/drawer-cover.png"
            }}
            style={{
              height: 120,
              width: "100%",
              alignSelf: "stretch",
              position: "absolute"
            }}
          />
          <Image
            square
            style={{
              height: 80,
              width: 70,
              position: "absolute",
              top: 20
            }}
            source={{
              uri:
                "https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/logo.png"
            }}
          />
          <List
            dataArray={routes}
            contentContainerStyle={{ marginTop: 120 }}
            renderRow={anchor => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(anchor)}
                >
                  <Text>{anchor}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}
