import React from 'react';
import {Container, Header, Title, Button, Left, Right, Body, Icon, View, Text} from 'native-base';

const Home = props => {
  const { navigation } = props;
  // navigateTo = (str) => () => navigation.navigate(str);
  // const list = ['Comp1', 'Comp2', 'Comp3', 'Comp4', 'Comp5', 'Comp6', 'Comp7']
  return (
    <Container>
      <Header>
        <Left>
          <Button
            transparent
            onPress={() => navigation.openDrawer()}
          >
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>HomeView</Title>
        </Body>
        <Right />
      </Header>
      <View>
        <Text>This is home page</Text>
      </View>
    </Container>
  );
};

export default Home;
