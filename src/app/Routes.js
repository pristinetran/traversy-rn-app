import React from 'react';
import {createDrawerNavigator} from 'react-navigation';
import Component1 from "../Comp1/components/Component1";
import Component2 from "../Comp2/components/Component2";
import Component3 from "../Comp3/components/Component3";
import Component4 from "../Comp4/components/Component4";
import Component5 from "../Comp5/components/Component5";
import Component6 from "../Comp6/components/Component6";
import Component7 from "../Comp7/components/Component7";
import Home from "./Home";
import SideBar from "./sidebar/SideBar";
const list = [
  {'Home': Home},
  {'Comp1': Component1},
  {'Comp2': Component2},
  {'Comp3': Component3},
  {'Comp4': Component4},
  {'Comp5': Component5},
  {'Comp6': Component6},
  {'Comp7': Component7}
]

const initialRoute = {
  initialRouteName: 'Home',
}

const navConfig = list.reduce((result, ele) => {
  const key = Object.keys(ele)[0];
  return { ...result,
    [key]: { screen: ele[key] }
  };
}, {});

export const routes = Object.keys(navConfig);

const Routes = createDrawerNavigator(
  navConfig,
  {
    contentComponent: props => <SideBar {...props} />
  }
);

export default Routes;
