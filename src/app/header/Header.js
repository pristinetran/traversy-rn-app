import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Body,
  Header,
  Left,
  Right,
  Icon,
  Title,
} from "native-base";

const HeaderComp = ({ onPressHandler, title }) => {
  return (
    <Header>
      <Left>
        <Button
          transparent
          onPress={onPressHandler}
        >
          <Icon name="menu" />
        </Button>
      </Left>
      <Body>
      <Title>{title}</Title>
      </Body>
      <Right />
    </Header>
  );
};

HeaderComp.propTypes = {

};

export default HeaderComp;
