import * as Types from './ActionTypes'

export const selectLibrary = (payload) => {
  return {
    type: Types.SELECT_LIBRARY,
    payload: payload
  };
};
